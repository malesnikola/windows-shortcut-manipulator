package main.java.util;

public class Constants {
    public static final String APP_VERSION = "2018.07.29";

    public static final String LNK_FILE_TYPE_EXTENSION = ".lnk";
    public static final String LNK_FILE_TYPE_DESCRIPTION = "Windows shortcut files (*.lnk)";
    public static final String MP3_FILE_TYPE_EXTENSION = ".mp3";
    public static final String MP3_FILE_TYPE_DESCRIPTION = "MP3 files (*.mp3)";
}
